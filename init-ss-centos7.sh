#!/bin/bash

if [[ $EUID -ne 0 ]]; then
    echo "this script must be run as a root user"
    exit 1
fi

cat <<EOF > /etc/systemd/system/shadowsocks.service
[Unit]
Description=service_description
After=network.target

[Service]
Type=forking
PIDFile=/etc/shadowsocks/ss.pid
ExecStart=/usr/local/bin/ss-server -c /etc/shadowsocks/config.json -a nobody -f /etc/shadowsocks/ss.pid -u
ExecStop=ps aux | grep "[s]s-server" | awk {"print $2"} | xargs kill -9
EOF

if [ ! -d "/etc/shadowsocks" ]; then
    mkdir /etc/shadowsocks
fi

if [ ! -f "/etc/shadowsocks/config.json" ]; then 
cat <<EOF > /etc/shadowsocks/config.json
{
    "server":"0.0.0.0",
    "server_port":9999,
    "password":"password",
    "timeout":60,
    "method":"aes-256-cfb",
    "workers":2
}
EOF
fi

systemctl daemon-reload

systemctl start shadowsocks
